package com.xinyue.fishinghunter.dao;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.xinyue.fishinghunter.dao.entity.FishingHunterPlayer;
import com.xinyue.fishinghunter.dao.redis.EnumRedisKey;
import com.xinyue.fishinghunter.dao.repository.FishingHunterPlayerRepository;
import com.xinyue.framework.dao.AbstractDao;
import com.xinyue.framework.dao.redis.IRedisKeyConfig;

@Service
public class FishingHunterPlayerDao extends AbstractDao<FishingHunterPlayer, Long> {
	@Autowired
	private FishingHunterPlayerRepository fishingHunterPlayerRepository;
	@Autowired
	private NacosDiscoveryProperties properties;
	
	@PostConstruct
	public void init() {
		EnumRedisKey.Namespace = properties.getNamespace();
		
	}

	@Override
	protected IRedisKeyConfig getRedisKey() {
		return EnumRedisKey.PLAYER_INFO;
	}

	@Override
	protected MongoRepository<FishingHunterPlayer, Long> getMongoRepository() {
		return fishingHunterPlayerRepository;
	}

	@Override
	protected Class<FishingHunterPlayer> getEntityClass() {
		return FishingHunterPlayer.class;
	}

}
