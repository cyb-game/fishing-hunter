package com.xinyue.fishinghunter.login.logic;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.xinyue.fishinghunter.dao.entity.FishingHunterPlayer;
import com.xinyue.fishinghunter.event.messages.CreatePlayerMQEvent;
import com.xinyue.fishinghunter.fish.FishServerErrorCode;
import com.xinyue.fishinghunter.fish.service.PlayerService;
import com.xinyue.fishinghunter.login.messages.CreatePlayerRequest;
import com.xinyue.fishinghunter.login.messages.CreatePlayerResponse;
import com.xinyue.fishinghunter.login.messages.EnterGameRequest;
import com.xinyue.fishinghunter.login.messages.EnterGameResponse;
import com.xinyue.game.logic.frame.GameServerContext;
import com.xinyue.game.logic.frame.handler.GameMessageHandler;
import com.xinyue.game.logic.frame.handler.GameRequestMapping;
import com.xinyue.mqsystem.event.GameMQEventContext;

@GameMessageHandler
public class LoginHandler {

	private Logger logger = LoggerFactory.getLogger(LoginHandler.class);
	@Autowired
	private PlayerService playerService;
	@Autowired
	private PlayerLoginService playerLoginService;

	@GameRequestMapping(CreatePlayerRequest.class)
	public void createPlayer(CreatePlayerRequest request, GameServerContext ctx) {
		// TODO 先简单实现一下，后面再修改
		// 判断角色是否已存在
		// 判断昵称是否存
		FishingHunterPlayer player = new FishingHunterPlayer();
		player.setPlayerId(ctx.getPlayerId());
		player.setNickname(request.getParams().getNickname());
		playerService.updatePlayer(player);
		String accountId = ctx.getAccountId();
		player.setAccountId(accountId);
		// 发送创建昵称的event
		CreatePlayerMQEvent createPlayerMQEvent = new CreatePlayerMQEvent();
		createPlayerMQEvent.getMessageBuilder().setAccountId(accountId).setNickname(player.getNickname());
		GameMQEventContext.sendEvent(createPlayerMQEvent);
		CreatePlayerResponse response = request.newCouple();
		response.getMessageBuilder().setPlayerId(player.getPlayerId());
		ctx.sendToGateway(response);
		logger.debug("{}创建角色{} {}成功", player.getAccountId(), player.getPlayerId(), player.getNickname());
	}

	@GameRequestMapping(EnterGameRequest.class)
	public void enterGame(EnterGameRequest request, GameServerContext ctx) {
		long playerId = ctx.getPlayerId();
		Optional<FishingHunterPlayer> playerOp = playerService.getPlayer(playerId);
		EnterGameResponse response = request.newCouple();
		if (playerOp.isPresent()) {
			FishingHunterPlayer player = playerOp.get();
			this.setInitValue(player);

			playerService.updatePlayer(player);
			String playerJson = JSON.toJSONString(player);
			response.getMessageBuilder().setPlayer(playerJson);
		} else {
			response.getHeader().setErrorCode(FishServerErrorCode.PLAYER_NOT_EXIST);
		}
		ctx.sendToGateway(response);

	}

	private void setInitValue(FishingHunterPlayer player) {

		if (player.getLevel() == 0) {
			player.setLevel(1);
		}
		if (player.getGold() == 0) {
			player.setGold(100);
		}
		if (player.getBullet() == 0) {
			player.setBullet(20);
		}
	}

}
