package com.xinyue.fishinghunter.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.xinyue.game.common.config.GameServerCommonConfig;
import com.xinyue.game.logic.frame.XinyueGameServerBoot;
import com.xinyue.mqsystem.event.GameMQEventContext;

@SpringBootApplication
@ComponentScan(basePackages = "com.xinyue.fishinghunter")
public class FishingHunterLoginServer {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(FishingHunterLoginServer.class, args);
		XinyueGameServerBoot.run(context, args);
		GameServerCommonConfig serverConfig = context.getBean(GameServerCommonConfig.class);
		// 启动事件系统
		GameMQEventContext.init(serverConfig.getLocalServerId(), context);
	}
}
