package com.xinyue.fishinghunter.fish.handlers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.xinyue.fishinghunter.dao.entity.FishingHunterPlayer;
import com.xinyue.fishinghunter.fish.FishServerErrorCode;
import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages;
import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.UpdatePlayerRequestParams;
import com.xinyue.fishinghunter.fish.messages.GetRankMessageRequest;
import com.xinyue.fishinghunter.fish.messages.GetRankMessageResponse;
import com.xinyue.fishinghunter.fish.messages.UpdatePlayerMessageRequest;
import com.xinyue.fishinghunter.fish.messages.UpdatePlayerMessageResponse;
import com.xinyue.fishinghunter.fish.service.PlayerService;
import com.xinyue.fishinghunter.fish.service.rank.EnumRankType;
import com.xinyue.fishinghunter.fish.service.rank.RankInfo;
import com.xinyue.fishinghunter.fish.service.rank.RankService;
import com.xinyue.game.logic.frame.GameServerContext;
import com.xinyue.game.logic.frame.handler.AbstractGameMessageHandler;
import com.xinyue.game.logic.frame.handler.GameMessageHandler;
import com.xinyue.game.logic.frame.handler.GameRequestMapping;

@GameMessageHandler
public class PlayerHandler extends AbstractGameMessageHandler {
	@Autowired
	private PlayerService playerService;
	@Autowired
	private RankService rankService;
	
	@GameRequestMapping(GetRankMessageRequest.class)
	public void getRank(GetRankMessageRequest request, GameServerContext ctx) {
		long playerId = ctx.getPlayerId();
		int type = request.getParams().getRankType();
		EnumRankType rankType = EnumRankType.getRankType(type);
		GetRankMessageResponse response = request.newCouple();
		if (rankType == null) {
			response.getHeader().setErrorCode(FishServerErrorCode.RANK_TYLE_ERROR);
		} else {
			int myRank = rankService.getMyRank(playerId, rankType);
			response.getMessageBuilder().setMyRank(myRank);
			Set<RankInfo> rankInfos = rankService.getRankInfos(rankType, 100);
			FishingHunterPlayer player = null;
			for (RankInfo rankInfo : rankInfos) {
				FishingHunterMessages.VoRankInfo.Builder voRankInfoBuilder = FishingHunterMessages.VoRankInfo.newBuilder();
				voRankInfoBuilder.setRank(rankInfo.getRank());
				player = playerService.getPlayer(Long.parseLong(rankInfo.getMember())).orElse(null);
				if (player != null) {
					voRankInfoBuilder.setImgUrl(player.getHeadImg());
					voRankInfoBuilder.setNickName(player.getNickname());
					voRankInfoBuilder.setValue((int) rankInfo.getValue());
					response.getMessageBuilder().addRankInfos(voRankInfoBuilder.build());
				}
			}
			
		}
		ctx.sendToGateway(response);
	}

	@GameRequestMapping(UpdatePlayerMessageRequest.class)
	public void updatePlayer(UpdatePlayerMessageRequest request, GameServerContext ctx) {
		long playerId = ctx.getPlayerId();
		FishingHunterPlayer player = playerService.getPlayer(playerId).get();
		UpdatePlayerRequestParams body = request.getParams();
		player.setBullet(body.getBullet());
		player.setGold(body.getGold());
		player.setScore(body.getScore());
		playerService.updatePlayer(player);
		rankService.updateRank(playerId, player.getScore(), EnumRankType.ScoreRank);
		UpdatePlayerMessageResponse response = request.newCouple();
		int myRank = rankService.getMyRank(playerId, EnumRankType.ScoreRank);
		response.getMessageBuilder().setMyRank(myRank);
		ctx.sendToGateway(response);
	}
}
