package com.xinyue.fishinghunter.fish.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.UpdatePlayerRequestParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;
@GameMessageMetadata(messageId = 1003, messageType = EnumMesasageType.REQUEST, serviceId = 1, desc = "更新player")
public class UpdatePlayerMessageRequest extends AbstractServiceGameMessage{

	private UpdatePlayerRequestParams params;
	@Override
	protected void parseFrom(byte[] bytes) throws InvalidProtocolBufferException {
		params = UpdatePlayerRequestParams.parseFrom(bytes);
	}
	
	public UpdatePlayerRequestParams getParams() {
		return params;
	}
	@Override
	protected IGameMessage newCoupleImpl() {
		
		return new UpdatePlayerMessageResponse();
	}


}
